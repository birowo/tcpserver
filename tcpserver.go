package tcpserver

import (
	"log"
	"net"
	"sync"
)

func Config[T any](network, addr string, routineNum, bfrSz int, hndlr func(net.Conn, []byte, int, T)) {
	lstnr, err := net.Listen(network, addr) //create TCP listener
	if err != nil {
		log.Fatalln(err)
	}
	bfrPool := &sync.Pool{
		New: func() interface{} {
			return make([]byte, bfrSz)
		},
	}
	for i := 0; i < routineNum; i++ {
		go accept(bfrPool, lstnr, hndlr)
	}
	println(network, "server start listen on address:", addr, "\n")
	select {}
}
func accept[T any](bfrPool *sync.Pool, lstnr net.Listener, hndlr func(net.Conn, []byte, int, T)) {
	conn, err := lstnr.Accept()
	go accept(bfrPool, lstnr, hndlr)
	if err != nil {
		log.Println(err)
		return
	}
	var bfr_ any
	defer func() {
		bfrPool.Put(bfr_)
		conn.Close()
		if rcvr := recover(); rcvr != nil {
			log.Println(err)
		}
	}()
	var void T
	bfr_ = bfrPool.Get()
	bfr := bfr_.([]byte)
	hndlr(conn, bfr, 0, void)
}
